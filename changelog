wmanager (0.2.1-14) unstable; urgency=medium

  * Remove the manual update-menus calls in the postinst and postrm
    scripts; the debhelper snippets will take care of this.
    Incidentally, the manual invocations break with "set -e" if
    update-menus is not installed; Closes: #810336

 -- Peter Pentchev <roam@ringlet.net>  Tue, 12 Jan 2016 13:21:33 +0200

wmanager (0.2.1-13) unstable; urgency=medium

  * Bump the debhelper compatibility level to 9:
    - use debhelper's invocation of dpkg-buildflags, drop the versioned
      build dependency on dpkg-dev
    - use dpkg-buildflags's hardening flags, drop the build dependency
      on hardening-includes
  * Add the 05-strdup patch, replacing two unguarded uses of strcpy()
    with strdup().  Arguably the strcpy() uses were safe enough, but
    there's no harm in using strdup() and removing a Lintian warning.
  * Bump Standards-Version to 3.9.6:
    - drop the DM-Upload-Allowed field
  * Split the DEP-3 author information into several Author fields for
    the patches with multiple authors.
  * Update the copyright file to the 1.0 format and bump the years of
    my Debian packaging copyright notice.
  * Rewrite wmanagerrc-update in pure Perl, replace the perl5 and chase
    dependencies with perl:Depends.  Incidentally, Closes: #808387
  * Drop the version from the 'menu' suggestion, satisfied in oldstable.
  * Add the debian/upstream/metadata file.
  * Add Multi-Arch: foreign to the binary package.
  * Drop the source compression options; dpkg-dev's defaults are good enough.
  * Add --parallel to the debhelper invocation.
  * Switch the Vcs-Git and Vcs-Browser URLs to my GitLab repository.
  * Do not use the full path to update-menus.

 -- Peter Pentchev <roam@ringlet.net>  Sat, 19 Dec 2015 16:23:54 +0200

wmanager (0.2.1-12) unstable; urgency=medium

  * Do not use the obsolete and incorrect full path to update-alternatives
    in wmanagerrc-update.  Closes: #769966
    Thanks, Guillem Jover <guillem@debian.org>

 -- Peter Pentchev <roam@ringlet.net>  Thu, 27 Nov 2014 10:52:01 +0200

wmanager (0.2.1-11) unstable; urgency=low

  * Update the copyright file to the latest DEP 5 candidate format.
  * Upload to unstable.

 -- Peter Pentchev <roam@ringlet.net>  Tue, 01 Mar 2011 20:18:01 +0200

wmanager (0.2.1-10) experimental; urgency=low

  * Switch to Git and point the Vcs-* fields to Gitorious.
  * Use bzip2 compression for the Debian tarball.
  * Bump the debhelper compatibility level to 8 with no changes.
  * Harden the build by default and use hardening-includes instead of
    the hardening-wrapper.
  * Use := instead of = to optimize some shell invocations in the rules file.
  * Bump the year on my copyright notice.

 -- Peter Pentchev <roam@ringlet.net>  Thu, 06 Jan 2011 14:50:28 +0200

wmanager (0.2.1-9) unstable; urgency=low

  * Bump Standards-Version to 3.9.1 with no changes.
  * Let wmanagerrc-update use update-alternatives --query, not --display,
    to fix its operation with non-English text.  Closes: #605043

 -- Peter Pentchev <roam@ringlet.net>  Tue, 30 Nov 2010 13:03:14 +0200

wmanager (0.2.1-8) unstable; urgency=low
  
  * Bump Standards-Version to 3.9.0 with no changes.
  * Convert to the 3.0 (quilt) source format.
  * Use dpkg-buildflags from dpkg-dev 1.15.7 to obtain the default values
    for CXXFLAGS, CPPFLAGS, and LDFLAGS.
  * Simplify the rules file by exporting variables into the environment.
  * Update the copyright file to the latest revision of the DEP 5 format and
    bump the years on my copyright format.
  * Shorten the Vcs-Browser URL.

 -- Peter Pentchev <roam@ringlet.net>  Tue, 29 Jun 2010 00:29:44 +0300

wmanager (0.2.1-7) unstable; urgency=low

  * Let wmanager-loop honor the WM environment variable, if set, to
    specify the first window manager to run.
  * Bump the debhelper version dependency to 7.0.50 because of the use
    of override targets.
  * Bump Standards-Version to 3.8.3 with no changes.
  * Switch the copyright file header from the Wiki page to DEP 5 and
    shorten the file by breaking out the GPL-2+ blurb into its own block.
  * Actually implement the "patch" and "unpatch" targets in the rules file
    since the dh(1) tool does not always provide them.
  * Convert the patch file headers to DEP 3.
  * Only enable the build hardening wrapper if "hardening" is specified
    in DEB_BUILD_OPTIONS.

 -- Peter Pentchev <roam@ringlet.net>  Tue, 08 Sep 2009 18:02:52 +0300

wmanager (0.2.1-6) unstable; urgency=low

  * Switch to quilt for patch management:
    - change the patches' naming scheme
    - reformat the comment headers
    - update the README.source file
    - use the quilt plugin for debhelper in the rules file
  * Refresh the copyright file a bit:
    - move the homepage from Original-Source-Location to the control file's
      Homepage field.
    - remove the redundant Packaged-By and Packaged-Date fields
    - bump the CopyrightFormat proposal revision a bit
    - bump the year of my copyright notice for the Debian packaging
  * Add misc:Depends to the binary package.
  * Bump Standards-Version to 3.8.2 with no changes.
  * Move the various lists of installed files to debian/wmanager.*
  * Update a comment in the rules file - GCC 4.3 is the default now :)
  * Remove some cruft from the rules file.
  * Minimize the rules file using debhelper override targets.
  * Add the Vcs-Svn and Vcs-Browser control fields.
  * Make the short description a noun phrase.

 -- Peter Pentchev <roam@ringlet.net>  Wed, 17 Jun 2009 12:27:01 +0300

wmanager (0.2.1-5) unstable; urgency=low

  * debian/rules
    - do not install the virtually empty FAQ file.  Closes: #489056
    - harden the build unless "nohardening" is in DEB_BUILD_OPTIONS
    - use "filter" instead of "findstring" to parse DEB_BUILD_OPTIONS

 -- Peter Pentchev <roam@ringlet.net>  Thu, 03 Jul 2008 18:05:02 +0300

wmanager (0.2.1-4) unstable; urgency=low

  * Convert the package build to debhelper, minimizing the rules file.
  * Convert the copyright file to the new format.
  * The wmanager home site has risen from the dead, so add a watchfile.
  * Convert the manual pages to mdoc format.
  * Make the support scripts a bit more portable.

  * debian/README.source
    - create this file and note the use of dpatch
  * debian/compat
    - create this file and specify compatibility level 7
  * debian/control
    - add a build-time dependency on debhelper 7
    - bump standards version to 3.8.0
      - README.source
    - drop the perl5 dependency - no more POD-to-man conversions
  * debian/copyright
    - converted to the new machine-readable format
  * debian/patches/04_makefile.dpatch
    - always use the C++ compiler warning flags
  * debian/postinst
    - add the #DEBHELPER# marker
  * debian/postrm
    - add the #DEBHELPER# marker
  * debian/rules
    - use the debhelper programs for most tasks
    - let dh_strip handle the "noopt" and "nostrip" DEB_BUILD_OPTIONS
    - use $(CURDIR) instead of `pwd`
  * debian/watch
    - create this file and do weird things to the "021" upstream version
  * debian/wmanager.1
    debian/wmanager-loop.1
    debian/wmanagerrc-update.1
    - rewrite the POD documentation into real mdoc manual pages
  * debian/wmanager.pod
    debian/wmanager-loop.pod
    debian/wmanagerrc-update.pod
    - remove these files after replacing them with mdoc versions
  * debian/wmanager-loop
    - look for other terminals if x-terminal-emulator is not found;
      this is a no-op for Debian systems, yet allows this script to be
      used unaltered on other OS's
    - fix a typo in the program name
    - add my copyright notice
  * debian/wmanagerrc-update
    - only use update-alternatives if it is actually available;
      this is a no-op for Debian systems, yet allows this script to be
      used unaltered on other OS's
    - add my copyright notice

 -- Peter Pentchev <roam@ringlet.net>  Wed, 02 Jul 2008 18:57:17 +0300

wmanager (0.2.1-3) unstable; urgency=low

  * New maintainer.  Closes: #455007.
  * Switch to dpatch.
  * Support "noopt" and "nostrip" in DEB_BUILD_OPTIONS.  Closes: #438265.
  * Install the HISTORY file as the upstream changelog.
  * Update Standards-Version to 3.7.3.
  * Since menu-2.1.26, install-menu is in /usr/bin, so change the path
    in debian/menu-method and bump the version in the menu dependency.
  * No longer ignore "make clean" errors.
  * Create the md5sums control file.
  * Fix a warning about a C++ string used as a C character string.
  * No need to pass -isp to dpkg-gencontrol any more.
  * Constify the arguments to WManager::_CutString().
  * If "werror" is specified in DEB_BUILD_OPTIONS, make all compiler
    warnings fatal.
  * Fix the FTBFS with GCC 4.3 by including <cstdlib>.  Closes: #417758.
  * Only link against libfltk, it will bring in everything it needs.

 -- Peter Pentchev <roam@ringlet.net>  Mon, 17 Dec 2007 16:54:52 +0200

wmanager (0.2.1-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Medium-urgency upload for RC bugfix.
  * Rebuild for the C++ ABI transition.  Closes: #328176.

 -- Steve Langasek <vorlon@debian.org>  Thu, 22 Sep 2005 02:15:48 -0700

wmanager (0.2.1-2) unstable; urgency=low

  * Rewrite an [A-Z]* glob to [[:upper:]]* to make builders with non-C
    locale happy. Closes: #298328.
  * New upstream website and email address. Closes: #298331.
  * Move away from wm-menu-config, switch prerm to postrm as suggested by
    menu manual. Closes: #297795.
  * Write shobby man page for wmanager(1). Closes: #265815.

 -- Tommi Virtanen <tv@debian.org>  Wed,  9 Mar 2005 00:28:13 +0200

wmanager (0.2.1-1) unstable; urgency=low

  * New upstream release
  * Build-depend on libfltk1.1-dev. Closes: #192257, #191982.
  * Provide a menu-method that updates /etc/X11/wmanagerrc, make wmanager
    fall back to that if ~/.wmanagerrc does not exist. Closes: #117591.
  * Contributions from Andreas Rottmann <rotty@debian.org>:
     - fixed debian/copyright to avoid lintian warning.
     - bumped Standards-Version.
     - adapt Interface destructor for fltk 1.1 (fixes segfault).

 -- Tommi Virtanen <tv@debian.org>  Sun, 11 May 2003 17:34:49 +0300

wmanager (0.2-8) unstable; urgency=low

  * Include Brandon's NMU stuff. Closes: #116130.
  * g++ 3.0 compilation fixes from James Troup <james@nocrew.org>.
    Closes: 116402.
  * wmanagerrc-update: user preferences weren't always first.

 -- Tommi Virtanen <tv@debian.org>  Tue,  6 Nov 2001 18:57:46 +0200

wmanager (0.2-7.1) unstable; urgency=low

  * NMU
  * src/Interface.cc: add #include <string.h> to fix build-failure on ia64
    (Closes :#116130)
  * debian/control:
    - bumped Standards-Version
    - removed dependencies on essential packages findutils, grep, and sed
  * debian/rules: don't ship upstream INSTALL document

 -- Branden Robinson <branden@debian.org>  Thu, 18 Oct 2001 14:49:14 -0500

wmanager (0.2-7) unstable; urgency=low

  * Add wmanager-loop, a nice thing to exec from your ~/.xsession.
    Added perl5 to build-deps for pod2man, added perl5 and sed to
    dependencies.
  * Add wmanagerrc-update, a tool to generate your ~/.wmanagerrc
    by yours truely. Added chase, findutils (xargs) and grep to
    dependencies.
  * Updated description to mention above tools.
  * Fix upstream web location.
  * Move to using libfltk1-dev, updated build-deps.
  * No longer needs -lMesaGL to compile, dropped from build-deps.

 -- Tommi Virtanen <tv@debian.org>  Wed, 20 Dec 2000 21:19:23 +0200

wmanager (0.2-6) unstable; urgency=low

  * Rebuild in chroot to get rid of broken shlib depends,
    glx pollution again.

 -- Tommi Virtanen <tv@debian.org>  Sat, 24 Jun 2000 17:03:10 +0300

wmanager (0.2-5) unstable; urgency=low

  * Rebuild to get rid of broken depends. My X 4.0
    testing got in shlibs and managed to slip by.

 -- Tommi Virtanen <tv@debian.org>  Mon, 15 May 2000 18:06:42 +0300

wmanager (0.2-4) unstable; urgency=low

  * Upgrade to policy 3.1.1
  * Rewrote rules file to get rid of debhelper.
  * Use dpkg-gencontrol -isp

 -- Tommi Virtanen <tv@debian.org>  Thu,  4 May 2000 22:20:02 +0300

wmanager (0.2-3) frozen unstable; urgency=low

  * Recompile to fix dependency libgl -> libgl1, this time free
    from glx pollution. Closes: #60252.

 -- Tommi Virtanen <tv@debian.org>  Sun, 12 Mar 2000 12:04:52 +0200

wmanager (0.2-2) frozen unstable; urgency=low

  * Added -lGL and friends in make LDFLAGS to get libgl1 dependency right.
    Fixes an important bug in potato, please let this in. Closes: #59594.

 -- Tommi Virtanen <tv@debian.org>  Sat,  4 Mar 2000 11:30:25 +0200

wmanager (0.2-1) unstable; urgency=low

  * New upstream source.

 -- Tommi Virtanen <tv@debian.org>  Thu, 18 Nov 1999 19:22:01 +0200

wmanager (0.1-1) unstable; urgency=low

  * Initial Release.

 -- Tommi Virtanen <tv@debian.org>  Thu, 30 Sep 1999 22:24:29 +0300


